#+DATE: Sunday, Jul 09 2017
#+DESCRIPTION: getting ssl cert


* Getting SSL certificate
  #+BEGIN_SRC bash
  sudo apt-get update
  sudo apt-get install software-properties-common
  sudo add-apt-repository ppa:certbot/certbot
  sudo apt-get update
  sudo apt-get install python-certbot-nginx 
  #+END_SRC
